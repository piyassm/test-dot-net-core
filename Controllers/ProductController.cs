﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TestDotNet.Models;
using TestDotNet.Models.Input;
using TestDotNet.Models.Output;

namespace TestDotNet.Controllers
{
    public class ProductController : Controller
    {
        private readonly ILogger<ProductController> _logger;
        private readonly AppDBContext _context;

        public ProductController(ILogger<ProductController> logger, AppDBContext context)
        {
            _logger = logger;
            _context = context;

        }

        public IActionResult Index()
        {
            if (!(from a1 in _context.Product select a1).Any())
            {

                var list_product = new List<Product>();
                for (var i = 1; i <= 12; i++)
                {
                    Random rnd = new Random();
                    int price = rnd.Next(100, 5000);
                    list_product.Add(new Product { Id = i, Name = "Test" + i, ImgUrl = string.Format(@"/img/Test_{0}.jpg", i), Price = price });
                }
                _context.Product.AddRange(list_product);
                _context.SaveChanges();
            }
            if (!(from a1 in _context.Rating select a1).Any())
            {
                var list_rating = new List<Rating>();
                var x = 1;
                var y = 1;
                for (var i = 1; i <= 50; i++)
                {
                    if (y == 11)
                    {
                        y = 1;
                    }
                    Random rnd = new Random();
                    int number = rnd.Next(1, 11);
                    if (number > 5)
                    {
                        int score = rnd.Next(1, 6);
                        list_rating.Add(new Rating { Id = x, ProductId = y, Score = score });
                        x++;
                    }
                    y++;
                }
                _context.Rating.AddRange(list_rating);
                _context.SaveChanges();
            }
            if (!(from a1 in _context.Discount select a1).Any())
            {
                var list_discount = new List<Discount>();
                var x = 1;
                for (var i = 1; i <= 12; i++)
                {
                    Random rnd = new Random();
                    int number = rnd.Next(1, 13);
                    if (number > 6)
                    {
                        int amont = rnd.Next(10, 96);
                        list_discount.Add(new Discount { Id = x, ProductId = i, Amount = amont });
                        x++;
                    }
                }
                _context.Discount.AddRange(list_discount);
                _context.SaveChanges();
            }
            var products = ProductList();
            return View(products);
        }

        public List<ProductOutput> ProductList()
        {
            var product = (from a1 in _context.Product
                           let d1 = _context.Discount.Where(x => a1.Id == x.ProductId).FirstOrDefault()
                           select new ProductOutput
                           {
                               Id = a1.Id,
                               Name = a1.Name,
                               Description = a1.Description,
                               ImgUrl = a1.ImgUrl,
                               Price = a1.Price,
                               Amount = d1 != null && d1.Amount > 0 ? d1.Amount : 0,
                           }).ToList();
            var id_product = (from a1 in product select a1.Id).ToList();
            var rating = (from a1 in _context.Rating where id_product.Contains(a1.ProductId) select a1).ToList();
            product.ForEach(x =>
            {
                var query_rating = (from a1 in rating where a1.ProductId == x.Id select a1);
                if (query_rating.Any())
                {
                    x.Score = query_rating.Sum(x => x.Score) / query_rating.Count();
                }
            });
            return product;
        }

        public IActionResult Detail(int id)
        {
            var products = ProductList();
            var product = (from a1 in products where a1.Id == id select a1).FirstOrDefault();
            return PartialView(product);
        }

        [HttpPost]
        public IActionResult AddProduct([FromBody] ProductInput product)
        {
            var running = _context.Cart.Count();
            var item = new Cart()
            {
                Id = running + 1,
                Amount = product.Amount,
                ProductId = product.ProductId,
            };
            _context.Add(item);
            _context.SaveChanges();
            return Json(item);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
