﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


$(function () {

    $('body').on('click', '.card-img-top.product', function (e) {
        e.preventDefault();

        $("#exampleModal").remove();

        $.get($(this).data("targeturl"), function (data) {

            $('<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                '<div class="modal-dialog modal-lg" role="document"><div class="modal-content">' +
                data + '</div></div></div>').modal();

        });
    });

    $('body').on('change', '#myNumber', function (e) {
        e.preventDefault();
        var $mynumber = $('#myNumber');
        if ($mynumber.val() < 1) {
            $mynumber.val(1)
        } else if ($mynumber.val() > 10) {
            $mynumber.val(10)
        }
    });

    $('body').on('click', '#down', function (e) {
        e.preventDefault();
        down('1')
    });

    $('body').on('click', '#up', function (e) {
        e.preventDefault();
        up('10')
    });

    $('body').on('click', '#add_product', function (e) {
        e.preventDefault();
        var $mynumber = $('#myNumber');
        var $myproduct = $('#product_id');

        var product = new Object();
        product.Amount = parseInt($mynumber.val());
        product.ProductId = parseInt($myproduct.val());
        if (product != null) {
            $.ajax({
                type: "POST",
                url: "/Product/AddProduct",
                data: JSON.stringify(product),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    alert("Ok");
                },
                failure: function (response) {
                    alert(response.responseText);
                },
                error: function (response) {
                    alert(response.responseText);
                }
            });
        }
    });
});


function up(max) {
    var $mynumber = $('#myNumber');
    $mynumber.val(parseInt($mynumber.val()) + 1)
    if ($mynumber.val() >= parseInt(max)) {
        $mynumber.val(max);
    }
}
function down(min) {
    var $mynumber = $('#myNumber');
    $mynumber.val(parseInt($mynumber.val()) - 1)
    if ($mynumber.val() <= parseInt(min)) {
        $mynumber.val(min);
    }
}
