﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestDotNet.Models.Input
{
    public class ProductInput
    {
        public int Amount { get; set; }
        public int ProductId { get; set; }
    }
}
